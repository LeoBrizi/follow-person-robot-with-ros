#include "follow_person/PersonCoordinates.h"
#include "geometry_msgs/Twist.h"
#include "ros/ros.h"
#include <stdio.h>

float maxLinearVelX = 2.0;
float maxVecAngularVelZ = 0.8;
float minDistPerson = 1;
float maxDistPerson = 8;
float maxPersonX = 300;
std::string cmvVelRosTopic = "cmd_vel";

ros::Publisher velPub;

void callback(const follow_person::PersonCoordinates::ConstPtr &msg_) {
  float vecLinearX = ((msg_->z - minDistPerson) * maxLinearVelX) / maxDistPerson;       // scalo il valore
  float vecAngularZ = ((msg_->x * maxVecAngularVelZ) / maxPersonX) * (-1); // scalo il valore
  if (msg_->z > maxDistPerson || msg_->z < 0)
    vecLinearX = 0;
  if (vecLinearX > 0 && vecLinearX < 0.1)
    vecLinearX = 0.1;
  if (vecLinearX < 0 && vecLinearX > -0.1)
    vecLinearX = - 0.1;
  if (msg_->z == -1) {
    vecLinearX = 0;
    vecAngularZ = 0;
  }
  geometry_msgs::Twist msg;
  msg.linear.x = vecLinearX;
  msg.angular.z = vecAngularZ;
  velPub.publish(msg);
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "follow_person");
  ros::NodeHandle nh;

  ros::Subscriber sub = nh.subscribe("person_coordinates", 100, callback);

  velPub = nh.advertise<geometry_msgs::Twist>(cmvVelRosTopic, 100);

  while (ros::ok())
    ros::spinOnce();

  return 0;
}