#include "follow_person/PersonCoordinates.h"
#include "ros/ros.h"
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <iostream>
#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/synchronizer.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <stdio.h>

using namespace sensor_msgs;
using namespace message_filters;

std::string imageDepthRosTopic = "/camera/depth/image_raw";
std::string imageRgbRosTopic = "/camera/rgb/image_color";

ros::Publisher coordinatesPub;

cv::String lowerbodyCascadeName = "/opt/ros/kinetic/share/OpenCV-3.3.1-dev/"
                                  "haarcascades/haarcascade_lowerbody.xml";
cv::CascadeClassifier lowerbodyCascade;

bool detection = false;
std::vector<cv::Point2f> features, tracks;
cv::Mat prevImg;

int displacementFactor = 20;

std::string window1Name = "videoRGB";
std::string window2Name = "videoDepth";
std::string windowToFollow = "toFollow";
std::string windowLeg = "legToFollow";

float points_dist(const cv::Point2f &p1, const cv::Point2f &p2);

void draw_points(cv::Mat &img, const std::vector<cv::Point2f> &pts,
                 cv::Scalar color);

void image_callback(const ImageConstPtr &depthImage_,
                    const ImageConstPtr &rgbImage_);
// detection
cv::Rect lowerbodies_detection(const cv::Mat &image);
// filtering
cv::Mat filtering_leg(const cv::Mat &imgGray_, const cv::Mat &scaledDepthImg_,
                      cv::Rect lowerbody_);
// calculate the centroid of features
cv::Point3f calculate_centroid(const std::vector<cv::Point2f> &features_,
                               const cv::Mat &scaledDepthImg_);

int main(int argc, char **argv) {
  if (!lowerbodyCascade.load(lowerbodyCascadeName)) {
    std::cerr << "--(!)Error loading haarcascade." << std::endl;
    return -1;
  }

  ros::init(argc, argv, "person_detector");
  ros::NodeHandle nh;

  message_filters::Subscriber<Image> depthSub(nh, imageDepthRosTopic, 1);
  message_filters::Subscriber<Image> rgbSub(nh, imageRgbRosTopic, 1);

  coordinatesPub =
      nh.advertise<follow_person::PersonCoordinates>("person_coordinates", 100);

  typedef sync_policies::ApproximateTime<Image, Image> syncPolicy;
  Synchronizer<syncPolicy> sync(syncPolicy(10), depthSub, rgbSub);
  sync.registerCallback(boost::bind(&image_callback, _1, _2));

  cv::namedWindow(window1Name);
  cv::startWindowThread();
  cv::namedWindow(window2Name);
  cv::startWindowThread();
  cv::namedWindow(windowToFollow);
  cv::startWindowThread();
  cv::namedWindow(windowLeg);
  cv::startWindowThread();

  while (ros::ok())
    ros::spinOnce();

  cv::destroyWindow(window1Name);
  cv::destroyWindow(window2Name);
  cv::destroyWindow(windowToFollow);
  cv::destroyWindow(windowLeg);
  return 0;
}

void image_callback(const ImageConstPtr &depthImage_,
                    const ImageConstPtr &rgbImage_) {
  cv_bridge::CvImagePtr rgbImage =
      cv_bridge::toCvCopy(rgbImage_, sensor_msgs::image_encodings::BGR8);
  // cv_bridge::CvImageConstPtr depthImage = cv_bridge::toCvShare(depthImage_,
  // sensor_msgs::image_encodings::TYPE_32FC1);
  cv_bridge::CvImageConstPtr rawDepthImage = cv_bridge::toCvShare(
      depthImage_, sensor_msgs::image_encodings::TYPE_16UC1);

  cv::Mat imgGray;
  cv::Mat scaledDepthImg;

  (rawDepthImage->image).convertTo(scaledDepthImg, CV_32F, 0.001);
  // Smooth the image to remove some noise
  cv::GaussianBlur(rgbImage->image, rgbImage->image, cv::Size(0, 0), 1);
  // Convert to grayscale
  cv::cvtColor(rgbImage->image, imgGray, CV_BGR2GRAY);
  // Apply Histogram Equalization
  cv::equalizeHist(imgGray, imgGray);

  if (!detection) {
    /*********** DETECTION ***********/
    // Find the most significant lowerbody
    cv::Rect lowerbody = lowerbodies_detection(imgGray);
    if (lowerbody.x != 0 && lowerbody.y != 0) {
      cv::rectangle(rgbImage->image, cv::Point(lowerbody.x, lowerbody.y),
                    cv::Point(lowerbody.x + lowerbody.width,
                              lowerbody.y + lowerbody.height),
                    cv::Scalar(0, 255, 0), 4, 8, 0);
      cv::imshow(windowToFollow, rgbImage->image);
      cv::waitKey(1);

      /*********** FEATURES EXTRACTION ***********/
      /* Prepare an unsigned char mask: 0 outside the area of leg, 255 inside,
       * to be used to detect features */
      cv::Mat mask = filtering_leg(imgGray, scaledDepthImg, lowerbody);
      cv::imshow(windowLeg, mask);
      cv::waitKey(1);
      /************************************************************************
       * IMPLEMENT THE DETECTION STEP, using the Shi and Tomasi corner detector
       * **********************************************************************/
      // trovo le features

      double qualityLevel = 0.1;
      int minDistance = 3;
      int maxFeature = 200;
      cv::goodFeaturesToTrack(mask, features, maxFeature, qualityLevel,
                              minDistance);

      /* We have detected the leg and detected the related features:
       * for the remainig images, just track these features */
      detection = true;
      std::cout << "le features all'interno delle gambe sono state trovate"
                << std::endl;
    }
  } else {
    // calculate the centroid of features
    cv::Point3f centroid = calculate_centroid(features, scaledDepthImg);
    if (int(features.size()) <= 5) {
      centroid = cv::Point3f(-1, -1, -1);
      detection = false;
      features.clear();
      return;
    }
    // send the centroid to ros node follow_person
    follow_person::PersonCoordinates msg;
    msg.x = centroid.x;
    msg.y = centroid.y;
    msg.z = centroid.z;
    coordinatesPub.publish(msg);

    std::vector<uchar> status;
    std::vector<float> errors;
    cv::Size trackingWinSize(31, 31);
    /* status – output status vector (of unsigned chars);
    * each element of the vector is set to 1 if the flow for the corresponding
    * features has been found,
    * otherwise, it is set to 0
    * output vector of errors; each element of the vector is set to an error for
    * the corresponding feature
    * winSize – size of the search window at each pyramid level */
    cv::calcOpticalFlowPyrLK(prevImg, imgGray, features, tracks, status,
                            errors, trackingWinSize, 3);
    /**********************************************************************
     * IMPLEMENT THE TRACKING STEP, using the features and tracks instances
     *********************************************************************/
    /* Now the current tracks will become the features points to be tracked for
    * the next
    * In the next code lines, i try to remove some outliers, i.e. some bad
    * tracks:
    * i compute the average (mean) optical flow dislacements, and discard all
    * the tracks with
    * displacement greater than 5X the current standard deviation*/

    std::vector<float> flowDisplacement;
    flowDisplacement.reserve(features.size());
    double mean = 0, stdDev = 0;
    int numValidTracks = 0;
    // Compute the displacement mean
    for (int i = 0; i < int(features.size()); i++) {
      float dist = 0;
      // The flow of the corresponding features has been found?
      if (status[i]) {
        dist = points_dist(features[i], tracks[i]);
        mean += dist;
        numValidTracks++;
      }
      flowDisplacement.push_back(dist);
    }
    if (!numValidTracks)
      numValidTracks++;
    mean /= numValidTracks; // averange

    // Compute the displacement standard deviation
    for (int i = 0; i < int(flowDisplacement.size()); i++) {
      if (status[i])
        stdDev += (flowDisplacement[i] - mean) * (flowDisplacement[i] - mean);
    }
    stdDev = std::sqrt(stdDev / numValidTracks);
    /* Select form the the current features only the ones that have deviation
     * less than 5X of standard deviation. */
    features.clear();
    for (int i = 0; i < int(flowDisplacement.size()); i++) {
      if (status[i] && fabs(flowDisplacement[i] - mean) < 5 * stdDev)
        features.push_back(tracks[i]);
    }
    tracks.clear();
  }

  draw_points(rgbImage->image, features, cv::Scalar(0, 0, 255));
  prevImg = imgGray;
  cv::imshow(window1Name, rgbImage->image);
  cv::imshow(window2Name, scaledDepthImg);
  cv::waitKey(1);
}

cv::Rect lowerbodies_detection(const cv::Mat &image) {
  std::vector<cv::Rect> lowerbodies;
  lowerbodyCascade.detectMultiScale(image, lowerbodies, 1.1, 2,
                                    0 | CV_HAAR_SCALE_IMAGE,
                                    cv::Size(200, 200)); 
  if (int(lowerbodies.size()) > 0) {
    cv::Rect biggestLowerBody;
    int maxArea = 0;
    for (int i = 0; i < int(lowerbodies.size()); i++) {
      if (lowerbodies[i].width * lowerbodies[i].height > maxArea) {
        maxArea = lowerbodies[i].width * lowerbodies[i].height;
        biggestLowerBody = lowerbodies[i];
      }
    }
    return biggestLowerBody;
  }
  return cv::Rect(0, 0, 0, 0);
}

cv::Mat filtering_leg(const cv::Mat &imgGray_, const cv::Mat &scaledDepthImg_,
                      cv::Rect lowerbody_) {
  cv::Mat cutScaledDepthImg =
      scaledDepthImg_(lowerbody_); // cut the leg from the scaled img
  cv::Rect rectangle =
      cv::Rect(0, 0, cutScaledDepthImg.cols,
               cutScaledDepthImg.rows - 50); 
  cutScaledDepthImg = cutScaledDepthImg(rectangle);
  double min, max;
  min = 1000;
  for (int r = 0; r < cutScaledDepthImg.rows; ++r)
    for (int c = 0; c < cutScaledDepthImg.cols; ++c) {
      if (cutScaledDepthImg.at<float>(r, c) != 0 and
          cutScaledDepthImg.at<float>(r, c) < min)
        min = cutScaledDepthImg.at<float>(r, c);
    }
  cv::Mat mask = cv::Mat::zeros(imgGray_.size(), cv::DataType<uchar>::type);
  for (int r = 0; r < cutScaledDepthImg.rows; ++r)
    for (int c = 0; c < cutScaledDepthImg.cols; ++c)
      if (cutScaledDepthImg.at<float>(r, c) <= min + 0.20 &&
          cutScaledDepthImg.at<float>(r, c) >= min - 0.20)
        mask.at<uchar>(r + lowerbody_.y,
                       c + lowerbody_.x - displacementFactor) = 255;
  // apply the mask to the gray image
  for (int r = 0; r < imgGray_.rows; ++r)
    for (int c = 0; c < imgGray_.cols; ++c)
      mask.at<uchar>(r, c) = imgGray_.at<uchar>(r, c) & mask.at<uchar>(r, c);
  return mask;
}

void draw_points(cv::Mat &img, const std::vector<cv::Point2f> &pts,
                 cv::Scalar color) {
  int ptsSize = int(pts.size()), width = img.cols, height = img.rows;
  for (int i = 0; i < ptsSize; i++) {
    int xc = cvRound(pts[i].x), yc = cvRound(pts[i].y);
    if (unsigned(xc) < unsigned(width) && unsigned(yc) < unsigned(height))
      cv::circle(img, cv::Point(xc, yc), 3, color, -1, 8);
  }
}

float points_dist(const cv::Point2f &p1, const cv::Point2f &p2) {
  return std::sqrt((p1.x - p2.x) * (p1.x - p2.x) +
                   (p1.y - p2.y) * (p1.y - p2.y));
}

cv::Point3f calculate_centroid(const std::vector<cv::Point2f> &features_,
                               const cv::Mat &scaledDepthImg_) {
  int x = 0, y = 0;
  float z = 0, depth;
  int p = 0;
  for (int i = 0; i < int(features_.size()); i++) {
    x += features_[i].x;
    y += features_[i].y;
    depth = 1000;
    for (int r = (features_[i].y - 5); r <= (features_[i].y + 5); ++r)
      for (int c = (features_[i].x - displacementFactor) - 5;
           c <= (features_[i].x - displacementFactor) + 5; ++c) { // x-25
        if (r < 0 || r > scaledDepthImg_.rows)
          continue;
        if (c < 0 || c > scaledDepthImg_.cols)
          continue;
        if (scaledDepthImg_.at<float>(r, c) != 0 and
            scaledDepthImg_.at<float>(r, c) < depth)
          depth = scaledDepthImg_.at<float>(r, c);
      }
    if (depth == 1000) {
      depth = 0;
      p++;
    }
    z += depth;
  }
  x = x / int(features_.size());
  y = y / int(features_.size());
  if (int(features_.size()) - p != 0)
    z = z / int((features_.size()) - p);
  else
    z = -1;
  // applico una trasformazione di coordinate per spostare il centro del sistema
  // di riferimento in (cols/2,rows/2,0)
  x = x - (scaledDepthImg_.cols / 2);
  y = (y - (scaledDepthImg_.rows / 2)) * (-1);
  // std::cout << "baricentro x: " << x << ", y: " << y << ", z: " << z
  //          << std::endl;
  return cv::Point3f(x, y, z);
}